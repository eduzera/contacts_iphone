//
//  ListaContatosProtocol.h
//  Contato
//
//  Created by ios2811 on 10/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ListaContatosProtocol <NSObject>

-(void)contatoAdicionado:(Contato *) contato;
-(void)contatoAtualizado:(Contato *) contato;

@end
