//
//  ListaContatosViewController.h
//  Contato
//
//  Created by ios2811 on 08/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FormularioContatoController.h"
#import "Contato.h"
#import "ListaContatosProtocol.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "Twitter/TWTweetComposeViewController.h"


@interface ListaContatosViewController : UITableViewController<ListaContatosProtocol, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

@property(strong) NSMutableArray *contatos;
@property(strong) Contato *contatoSelecionado;

-(void)exibeMaisAcoes:(UIGestureRecognizer *) gesture;

@end
