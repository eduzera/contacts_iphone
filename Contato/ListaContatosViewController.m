//
//  ListaContatosViewController.m
//  Contato
//
//  Created by ios2811 on 08/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListaContatosViewController.h"
#import "ContatoSiteViewController.h"

@implementation ListaContatosViewController

@synthesize contatos;
@synthesize contatoSelecionado;


- (id)init {
    self = [super init];
    if (self) {
        UIImage *imagemTabItem = [UIImage imageNamed: @"lista-contatos.png"];
        UITabBarItem *tabItem = [[UITabBarItem alloc] initWithTitle:@"Contatos" image:imagemTabItem tag:0];
        self.tabBarItem = tabItem;
        self.navigationItem.title = @"Contatos";
//        self.navigationItem.title = @"Listagem";
        UIBarButtonItem *botaoExibirFormulario = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(exibeFormulario)];
        self.navigationItem.rightBarButtonItem = botaoExibirFormulario;
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
    }
    return self;
} 

-(void) exibeFormulario {
    FormularioContatoController *form = [[FormularioContatoController alloc] init];
    form.contatos = self.contatos;
    form.delegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:form];
    [nav setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentModalViewController:nav animated:YES];
}

-(void) viewWillAppear:(BOOL)animated{
    NSLog(@"Total cadastrado: %d", [self.contatos count]);
    [self.tableView reloadData];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.contatos count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Contato *contato = [self.contatos objectAtIndex:indexPath.row];
    cell.textLabel.text = contato.nome;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [self.contatos removeObjectAtIndex:indexPath.row];
        NSArray *indexPaths = [NSArray arrayWithObject:indexPath];
        [tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Contato *contato = [self.contatos objectAtIndex:indexPath.row];
    FormularioContatoController *form = [[FormularioContatoController alloc] initWithContato:contato];
    form.delegate = self;
    form.contatos = self.contatos;
    [self.navigationController pushViewController:form
                                         animated:YES];
}

-(void) contatoAdicionado:(Contato *)contato{
    NSLog(@"adicionado: %d", [self.contatos indexOfObject:contato]);
}

-(void) contatoAtualizado:(Contato *)contato{
    NSLog(@"atualizado: %d", [self.contatos indexOfObject:contato]);
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(exibeMaisAcoes:)];
    
    [self.tableView addGestureRecognizer:longPress];
}

-(void) exibeMaisAcoes:(UIGestureRecognizer *)gesture{
    if(gesture.state == UIGestureRecognizerStateBegan){
        CGPoint ponto = [gesture locationInView:self.tableView];
        NSIndexPath *index = [self.tableView indexPathForRowAtPoint:ponto];
        
        Contato *contato = [contatos objectAtIndex:index.row];
        contatoSelecionado = contato;
        
        UIActionSheet *opcoes = [[UIActionSheet alloc] initWithTitle:contato.nome delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Ligar",@"Enviar Email", @"Visualizar Site",@"Abrir Mapa", @"Twitter" , nil];
        [opcoes showInView:self.view];
    }
}

-(void)abrirAplicativoComURL:(NSString *) url
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void) ligar{
    UIDevice *device = [UIDevice currentDevice];
    if([device.model isEqualToString:@"iPhone"]){
        NSString *numero = [NSString stringWithFormat:@"tel:%",contatoSelecionado.tel];
        [self abrirAplicativoComURL:numero];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Impossivel fazer ligacao"  message:@"Seus dispositivo nao e um ipphone" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
}

-(void)abrirSite{
//    NSString *url = contatoSelecionado.site;
//    [self abrirAplicativoComURL:url];
    ContatoSiteViewController *site = [[ContatoSiteViewController alloc] initWithContato:self.contatoSelecionado];

    [self.navigationController pushViewController:site animated:YES];
}

-(void) mostrarMapa{
    NSString *url = [[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@", contatoSelecionado] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self abrirAplicativoComURL:url];
}

-(void) enviarEmail{
    if([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *enviadorEmail = [[MFMailComposeViewController alloc] init];
        
        enviadorEmail.mailComposeDelegate = self;
        
        [enviadorEmail setToRecipients:[NSArray arrayWithObject:contatoSelecionado.email]];
        [enviadorEmail setSubject:@"Caelum"];
        
        [self presentModalViewController:enviadorEmail animated:YES];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops" message:@"You cannot send an email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void) twittar{
    TWTweetComposeViewController *twitter_page = [[TWTweetComposeViewController alloc] init];
    [twitter_page setInitialText:[NSString stringWithFormat:@"@%@ @caelum #ip67caelum @dchohfi teste app iphone",contatoSelecionado.twitter]];
    
    [self presentModalViewController:twitter_page animated:YES];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self ligar];
            break;
        case 1 :
            [self enviarEmail];
            break;
        case 2:
            [self abrirSite];
            break;
        case 3:
            [self mostrarMapa];
            break;
        case 4:
            [self twittar];
            break;
            
        default:
            break;
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissModalViewControllerAnimated:YES];
}

@end
