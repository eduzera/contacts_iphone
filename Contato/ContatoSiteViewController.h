//
//  ContatoSiteViewController.h
//  Contato
//
//  Created by ios2811 on 15/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contato.h"

@interface ContatoSiteViewController : UIViewController

@property(nonatomic,weak) IBOutlet UIWebView *web;
@property(strong) Contato *contato;

-(id)initWithContato:(Contato *)c;
@end
