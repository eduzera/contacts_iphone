//
//  ContatosNoMapaViewController.h
//  Contato
//
//  Created by ios2811 on 14/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapView.h>

@interface ContatosNoMapaViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapa;
@property (nonatomic,strong) NSMutableArray *contatos;

@end
