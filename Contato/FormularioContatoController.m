//
//  FormularioContatoController.m
//  Contato
//
//  Created by ios2811 on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FormularioContatoController.h"
#import "Contato.h"
#import "CoreLocation/CLGeocoder.h"
#import "CoreLocation/CLPlacemark.h"

@implementation FormularioContatoController

@synthesize nome, tel, email, endereco, site, contatos, contato, twitter, botaoFoto;
@synthesize latitude;
@synthesize longitude;
@synthesize delegate;
@synthesize loading;
@synthesize botaoGeo;
@synthesize campoAtual;
@synthesize tamanhoInicialDoScroll;

-(id) init{
    self = [super init];
    if (self) {
        self.navigationItem.title = @"Cadastro";
        
        UIBarButtonItem *cancelar = [[UIBarButtonItem alloc] initWithTitle:@"Cancela" style:UIBarButtonItemStylePlain target:self action:@selector(escondeFormulario)];
        
        UIBarButtonItem *adicionar = [[UIBarButtonItem alloc] initWithTitle:@"Adiciona" style:UIBarButtonItemStylePlain target:self action:@selector(criaContato)];
        
        self.navigationItem.leftBarButtonItem = cancelar;
        self.navigationItem.rightBarButtonItem = adicionar;
    }
    return self;
}

-(id)initWithContato:(Contato *)_contato{
    self = [super init];
    if(self){
        self.contato = _contato;
        
        UIBarButtonItem *confirmar = [[UIBarButtonItem alloc] initWithTitle:@"Confirmar" style:UIBarButtonItemStylePlain target:self action:@selector(atualizaContato)];
        self.navigationItem.rightBarButtonItem = confirmar;
    }
    return self;
}

-(Contato *)pegaDadosFormulario{
    NSMutableDictionary *dadosDoContato = [[NSMutableDictionary alloc] init];
    [dadosDoContato setObject:[nome text] forKey:@"nome"];
    [dadosDoContato setObject:[tel text] forKey:@"tel"];
    [dadosDoContato setObject:[email text] forKey:@"email"];
    [dadosDoContato setObject:[endereco text] forKey:@"endereco"];
    [dadosDoContato setObject:[site text] forKey:@"site"];
    [dadosDoContato setObject:[twitter text] forKey:@"twitter"];
    
    
    NSLog(@"dados: %@", dadosDoContato);
    
    if(!self.contato){
     contato = [[Contato alloc] init];   
    }
    
    if( botaoFoto.imageView.image ){
        contato.foto = botaoFoto.imageView.image;
    }
    
    [contato setNome:[nome text]];
    [contato setTel:[tel text]];
    [contato setSite:[site text]];
    [contato setEndereco:[endereco text]];
    [contato setEmail:[email text]];
    [contato setTwitter:[twitter text]];
    [contato setLatitude:[NSNumber numberWithFloat:[latitude.text floatValue]]];
    [contato setLongitude:[NSNumber numberWithFloat:[longitude.text floatValue]]];
    
    NSLog(@"contatos: %@", self.contatos);
    
//    [nome setText:@""];
//    [tel setText:@""];
//    [endereco setText:@""];
//    [email setText:@""];
//    [site setText:@""];
    
    NSLog(@"contato com nome %@", [contato nome]);
    
    return contato;
}

-(IBAction)escondeTeclado:(id)sender
{
    if (sender == nome) {
        [tel becomeFirstResponder];
    }else if(sender == tel){
        [email becomeFirstResponder];
    }else if(sender == email){
        [twitter becomeFirstResponder];
    }else if(sender == twitter){
        [endereco becomeFirstResponder];
    }else if(sender == endereco){
        [site becomeFirstResponder];
    }else if(sender == site){
        [latitude becomeFirstResponder];
    }else if(sender == latitude){
        [longitude becomeFirstResponder];
    }else{
        [self.view endEditing:YES];
    }
}

-(IBAction)escondeFormulario{
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)criaContato{
    Contato *novoContato = [self pegaDadosFormulario];
    [self.contatos addObject:novoContato];
    [self dismissModalViewControllerAnimated:YES];
    if (self.delegate) {
        [self.delegate contatoAdicionado:novoContato];
    }
}

-(void) viewDidLoad{
//    tamanhoInicialDoScroll = self.view.frame.size;
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tecladoApareceu:) name:UIKeyboardDidShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tecladoSumiu:) name:UIKeyboardWillHideNotification object:nil];
    
    if(self.contato){
        nome.text = contato.nome;
        tel.text = contato.tel;
        email.text = contato.email;
        endereco.text = contato.endereco;
        site.text = contato.site;
        twitter.text = contato.twitter;
        if(contato.foto){
            [botaoFoto setImage:contato.foto forState:UIControlStateNormal];
        }
        latitude.text = [contato.latitude stringValue];
        longitude.text = [contato.longitude stringValue];
    }
}

-(void)atualizaContato{
    Contato *contatoAtualizado = [self pegaDadosFormulario];
    [self.navigationController popViewControllerAnimated:YES];
    if (self.delegate) {
        [self.delegate contatoAtualizado:contatoAtualizado];
    }
}

-(IBAction)selecionaFoto:(id)sender{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        //camera disponivel
    }else{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
        picker.delegate = self;
        [self presentModalViewController:picker animated:YES];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *imagemSelecionada = [info valueForKey:UIImagePickerControllerEditedImage];
    [botaoFoto setImage:imagemSelecionada forState:UIControlStateNormal];
    [picker dismissModalViewControllerAnimated:YES];
    contato.foto = imagemSelecionada;
}


- (void)viewDidUnload {
    
    [self setLoading:nil];
    [self setBotaoGeo:nil];
    [super viewDidUnload];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    campoAtual = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    campoAtual = nil;
}

-(void)tecladoApareceu:(NSNotification *)notification {
	NSDictionary *info = [notification userInfo];
	
	//acessando o tamanho do teclado e a sua altura
	CGRect areaDoTeclado = [[info 
                             objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
	CGSize tamanhoTeclado = areaDoTeclado.size;
	
	//fazendo o cast da propriedade ::view:: para uma ::UIScrollView::
	//podemos fazer isso pois alteramos a classe da ::view:: pelo 
	//::Interface Builder::
	UIScrollView *scroll = (UIScrollView *)self.view;
	
	//calculando a márgem extra que daremos para o scroll
	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, tamanhoTeclado.height, 0.0);
	scroll.contentInset = contentInsets;
	scroll.scrollIndicatorInsets = contentInsets;
	
	if (campoAtual != NULL) {
		//calculando o tamanho adicional dos elementos
		//no caso o teclado e a navigationBar
		CGFloat tamanhoDosElementos = tamanhoTeclado.height + 
        self.navigationController.navigationBar.frame.size.height;
        
		//removendo o espaço extra dos elementos
		CGRect tamanhoDaTela = self.view.frame;
		tamanhoDaTela.size.height -= tamanhoDosElementos;
        
		//validando se o teclado foi escondido ou não
		BOOL campoAtualSumiu = !CGRectContainsPoint(tamanhoDaTela, campoAtual.frame.origin);
		if (campoAtualSumiu) {
			//caso o teclado tenha sido escondido, vamos adicionar
			//o tamanho do teclado adicional ao ::scroll view::.
			CGFloat tamanhoAdicional = tamanhoTeclado.height - 
            self.navigationController.navigationBar.frame.size.height;
            
			CGPoint pontoVisivel = CGPointMake(0.0, 
                                               campoAtual.frame.origin.y - tamanhoAdicional);
			[scroll setContentOffset:pontoVisivel animated:YES];
            
			CGSize scrollContentSize = scroll.contentSize;
			scrollContentSize.height += tamanhoAdicional;
			[scroll setContentSize:scrollContentSize];
		}
	}
}

-(void)tecladoSumiu:(NSNotification *)notification {
	UIScrollView *scroll = (UIScrollView *)self.view;
	UIEdgeInsets contentInsets = UIEdgeInsetsZero;
	scroll.contentInset = contentInsets;
	scroll.scrollIndicatorInsets = contentInsets;
	[scroll setContentOffset:CGPointZero animated:YES];
}

-(IBAction)buscaCoordenadas:(id)sender{
    [self.loading startAnimating];
    botaoGeo.hidden = YES;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:endereco.text completionHandler:^(NSArray *resultados, NSError *error){
        if(error == nil && [resultados count] > 0){
            CLPlacemark *resultado = [resultados objectAtIndex:0];
            CLLocationCoordinate2D coordenada = resultado.location.coordinate;
            latitude.text = [NSString stringWithFormat:@"%f", coordenada.latitude];
            longitude.text = [NSString stringWithFormat:@"%f",coordenada.longitude];
        }
        [self.loading stopAnimating];
        botaoGeo.hidden = NO;
    }];
}

@end
