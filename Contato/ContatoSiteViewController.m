//
//  ContatoSiteViewController.m
//  Contato
//
//  Created by ios2811 on 15/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContatoSiteViewController.h"


@implementation ContatoSiteViewController

@synthesize web;
@synthesize contato;

-(id)initWithContato:(Contato *)c{
    self = [super init];
    if (self) {
        self.navigationItem.title = [NSString stringWithFormat:@"Site do %@", c.nome];
        self.contato = c;
    }
    return self;
} 

-(void)viewDidLoad{
    NSString *siteContato = contato.site;
    NSURL *url = [NSURL URLWithString:siteContato];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:url];
    
    [self.web loadRequest:req];
}


@end
