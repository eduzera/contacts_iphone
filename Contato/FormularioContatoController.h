//
//  FormularioContatoController.h
//  Contato
//
//  Created by ios2811 on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contato.h"
#import "ListaContatosProtocol.h"


@interface FormularioContatoController : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate, UITextFieldDelegate>

@property(nonatomic,weak) IBOutlet UITextField *nome;
@property(nonatomic,weak) IBOutlet UITextField *tel;
@property(nonatomic,weak) IBOutlet UITextField *email;
@property(nonatomic,weak) IBOutlet UITextField *endereco;
@property(nonatomic,weak) IBOutlet UITextField *site;
@property(nonatomic,weak) IBOutlet UITextField *twitter;
@property(nonatomic,weak) IBOutlet UIButton *botaoFoto;
@property (weak, nonatomic) IBOutlet UITextField *latitude;
@property (weak, nonatomic) IBOutlet UITextField *longitude;
@property(strong) UITextField *campoAtual;
@property CGSize tamanhoInicialDoScroll;

@property(strong) NSMutableArray *contatos;
@property(strong) Contato *contato;

@property(strong) id<ListaContatosProtocol> delegate;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UIButton *botaoGeo;

-(id)initWithContato:(Contato *)contato;

-(IBAction)escondeTeclado:(id)sender;

-(IBAction)selecionaFoto:(id)sender;

-(IBAction)buscaCoordenadas:(id)sender;

@end
