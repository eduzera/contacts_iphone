//
//  AppDelegate.h
//  Contato
//
//  Created by ios2811 on 07/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong) NSMutableArray *contatos;
@property(strong) NSString *arquivoContatos;
@property(readonly, strong) NSManagedObjectContext *contexto;

@end
